defmodule QuotesServerTest do
  use ExUnit.Case
  doctest Meshager.QuotesServer.Impl

  setup_all do
    quotes = [
      %Meshager.QuotesServer.Impl{
        username: "username1",
        quote: "quote1",
        submitter: "sender1",
        timestamp: ~U[2000-01-01 00:00:00Z]
      },
      %Meshager.QuotesServer.Impl{
        username: "username2",
        quote: "quote2",
        submitter: "sender2",
        timestamp: ~U[2000-01-02 00:00:00Z]
      }
    ]

    {:ok, quotes: quotes}
  end

  test "can add a quote", context do
    new_quote = %Meshager.QuotesServer.Impl{
      username: "username3",
      quote: "quote3",
      submitter: "sender3",
      timestamp: ~U[2000-01-03 00:00:00Z]
    }

    quotes = Meshager.QuotesServer.Impl.add(new_quote, context[:quotes])

    assert quotes == [new_quote] ++ context[:quotes]
  end

  test "can format quotes", context do
    formatted = Meshager.QuotesServer.Impl.format_list(context[:quotes])

    assert formatted ==
             "on 2000-01-01 00:00:00Z, username1 said quote1,\non 2000-01-02 00:00:00Z, username2 said quote2"
  end
end
