defmodule Meshager do
  @moduledoc """
  Documentation for `Meshager`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Meshager.hello()
      :world

  """
  def hello do
    :world
  end
end
