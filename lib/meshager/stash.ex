defmodule Meshager.Stash do
  use GenServer

  @me __MODULE__

  @doc """
  Starts a stash.

  `:filename` is a path to a DETS file where state will be persisted.
  """
  def start_link(opts) do
    filename = Keyword.fetch!(opts, :filename)
    GenServer.start_link(__MODULE__, filename, name: @me)
  end

  def get_quotes() do
    GenServer.call(@me, {:get_quotes})
  end

  def update_quotes(quotes) do
    GenServer.cast(@me, {:update_quotes, quotes})
  end

  def get_latest_youtube_id() do
    GenServer.call(@me, {:get_latest_youtube_id})
  end

  def add_youtube_id(id) do
    GenServer.cast(@me, {:add_youtube_id, id})
  end

  # Server implementation
  def init(filename) do
    table = make_ref()
    :dets.open_file(table, file: to_charlist(filename), type: :set, auto_save: 1000)
    {:ok, table}
  end

  def handle_call({:get_quotes}, _from, table) do
    quotes =
      :dets.lookup(table, "quotes")
      |> quotes_or_default()

    {:reply, quotes, table}
  end

  defp quotes_or_default([{_, quotes} | _]) do
    quotes
  end

  defp quotes_or_default(_) do
    []
  end

  def handle_cast({:update_quotes, quotes}, table) do
    :dets.insert(table, {"quotes", quotes})
    {:noreply, table}
  end

  def handle_call({:get_latest_youtube_id}, _from, table) do
    id =
      :dets.lookup(table, "youtube_ids")
      |> latest_or_default()

    {:reply, id, table}
  end

  defp latest_or_default([{_, [id | _]} | _]) do
    id
  end

  defp latest_or_default(_) do
    ""
  end

  def handle_cast({:add_youtube_id, id}, table) do
    [{_, ids}] = :dets.lookup(table, "youtube_ids")

    :dets.insert(table, {"youtube_ids", [id | ids]})

    {:noreply, table}
  end
end
