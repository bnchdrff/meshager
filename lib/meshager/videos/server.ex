defmodule Meshager.Videos.Server do
  use GenServer

  alias Meshager.Stash
  alias Meshager.Videos.Impl

  # GenServer implementation
  def init(_) do
    {:ok, [Stash.get_latest_youtube_id()]}
  end

  def handle_call(:get, _from, ids) do
    {:reply, hd(ids), ids}
  end

  def handle_cast({:add, youtube_id}, ids) do
    new_ids = Impl.add(youtube_id, ids)
    Stash.add_youtube_id(youtube_id)
    {:noreply, new_ids}
  end
end
