defmodule Meshager.Videos.Impl do
  @spec add(youtube_id :: String.t(), ids :: [String.t()]) :: {:ok, [String.t()]}
  def add(youtube_id, ids) do
    new_ids = [youtube_id | ids]

    new_ids
  end
end
