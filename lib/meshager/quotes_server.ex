defmodule Meshager.QuotesServer do
  @server Meshager.QuotesServer.Server

  # External API
  def start_link(_) do
    GenServer.start_link(@server, nil, name: @server)
  end

  def list() do
    GenServer.call(@server, :list)
  end

  def add(quote) do
    GenServer.cast(@server, {:add, quote})
  end

  def child_spec(_) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [nil]}
    }
  end
end
