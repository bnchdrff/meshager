defmodule Meshager.QuotesServer.Server do
  use GenServer

  alias Meshager.Stash
  alias Meshager.QuotesServer.Impl

  # GenServer implementation
  def init(_) do
    {:ok, Stash.get_quotes()}
  end

  def handle_call(:list, _from, quotes) do
    {:reply, quotes, quotes}
  end

  def handle_cast({:add, quote}, quotes) do
    new_quotes = Impl.add(quote, quotes)
    Stash.update_quotes(new_quotes)
    {:noreply, new_quotes}
  end

  def terminate(_reason, quotes) do
    Stash.update_quotes(quotes)
  end
end
