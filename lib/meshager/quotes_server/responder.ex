defmodule Meshager.QuotesServer.Responder do
  use Igor.Responder.ModuleHelper

  @help %{
    command: "sq",
    args: "[username, quote]",
    short: "saves a quote",
    long: "Saves a memorable quote to the list."
  }
  command ["sq", username, quote], %Igor.Message{sender: sender} = orig_msg, bot do
    new_quote = %Meshager.QuotesServer.Impl{
      username: username,
      quote: quote,
      submitter: sender,
      timestamp: DateTime.utc_now()
    }

    Meshager.QuotesServer.add(new_quote)

    [Polyjuice.Client.MsgBuilder.mention(sender), "saved quote u: #{username} q: #{quote}"]
    |> Igor.send(orig_msg, bot)
  end

  @help %{
    command: "lq",
    short: "lists quotes",
    long: "Shows all saved quotes."
  }
  command ["lq"], %Igor.Message{sender: _sender} = orig_msg, bot do
    Meshager.QuotesServer.list()
    |> Meshager.QuotesServer.Impl.format_list()
    |> Igor.send(orig_msg, bot)
  end

  @help %{
    command: "yt",
    args: "[youtube_ish]",
    short: "display a youtube video on meshaging.com",
    long: "provide a youtube video id or a complete URL"
  }
  command ["yt", youtube_ish], %Igor.Message{sender: sender} = orig_msg, bot do
    %{"youtube_id" => youtube_id} =
      Regex.named_captures(~r/(?<youtube_id>[A-Za-z0-9_-]{11})/, youtube_ish)

    Registry.dispatch(Registry.Meshager, "/ws/videos", fn entries ->
      for {pid, _} <- entries do
        if pid != self() do
          Process.send(pid, youtube_id, [])
        end
      end
    end)

    Meshager.Videos.add(youtube_id)

    [Polyjuice.Client.MsgBuilder.mention(sender), "set youtube video: #{youtube_id}"]
    |> Igor.send(orig_msg, bot)
  end
end
