defmodule Meshager.QuotesServer.Impl do
  @type quote :: {
          timestamp :: DateTime.t(),
          username :: String.t(),
          quote :: String.t(),
          submitter :: String.t()
        }
  defstruct [:timestamp, :username, :quote, :submitter]

  @type quotes :: [quote]

  @spec add(quote :: quote, quotes :: quotes) :: {:ok, quotes}
  def add(quote, quotes) do
    new_quotes = [quote | quotes]

    # Try broadcasting to websocket

    new_quotes
  end

  @spec format_quote(quote :: quote) :: String.t()
  def format_quote(quote) do
    "on #{quote.timestamp}, #{quote.username} said #{quote.quote}"
  end

  @spec format_list(quotes :: quotes) :: String.t()
  def format_list(quotes) do
    quotes
    |> Enum.map(&format_quote(&1))
    |> Enum.join(",\n")
  end
end
