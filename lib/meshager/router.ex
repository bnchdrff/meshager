defmodule Meshager.Router do
  use Plug.Router
  require EEx

  plug(Plug.Logger)

  plug(Plug.Static,
    at: "/",
    from: :meshager
  )

  plug(:match)

  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason
  )

  plug(:dispatch)

  EEx.function_from_file(:defp, :index_html, "lib/index.html.eex", [:youtube_id])

  get "/" do
    send_resp(conn, 200, index_html(Meshager.Videos.get()))
  end

  match(_, do: send_resp(conn, 404, "Not found"))
end
