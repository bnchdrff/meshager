defmodule Meshager.Videos do
  @server Meshager.Videos.Server

  # External API
  def start_link(_) do
    GenServer.start_link(@server, nil, name: @server)
  end

  def get() do
    GenServer.call(@server, :get)
  end

  def add(youtube_id) do
    GenServer.cast(@server, {:add, youtube_id})
  end

  def child_spec(_) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [nil]}
    }
  end
end
