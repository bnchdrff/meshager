defmodule Meshager.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Starts a worker by calling: Meshager.Worker.start_link(arg)
      # {Meshager.Worker, arg}
      {Igor, Application.get_all_env(:igor)},
      {Meshager.Stash, [filename: "meshages.dets"]},
      {Meshager.QuotesServer, nil},
      {Meshager.Videos, nil},
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: Meshager.Router,
        options: [
          port: 3000,
          dispatch: dispatch()
        ]
      ),
      Registry.child_spec(
        keys: :duplicate,
        name: Registry.Meshager
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :rest_for_one, name: Meshager.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp dispatch do
    [
      {:_,
       [
         {"/ws/[...]", Meshager.SocketHandler, []},
         {:_, Plug.Cowboy.Handler, {Meshager.Router, []}}
       ]}
    ]
  end
end
