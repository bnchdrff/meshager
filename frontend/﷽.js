const socket = new WebSocket("ws://localhost:4000/ws/chat")

socket.addEventListener("message", (event) => {
	const pTag = document.createElement("p")
	pTag.innerHTML = event.data

	document.getElementById("main").append(pTag)
})

this.socket.addEventListener("close", () => {
	this.setupSocket()
})
