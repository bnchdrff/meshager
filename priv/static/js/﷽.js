function setupSocket() {
  const socket = new WebSocket("wss://meshaging.com/ws/videos");

  socket.addEventListener("message", ev => {
    document.getElementById("vid").innerHTML = `<iframe src="https://www.youtube.com/embed/${ev.data}" frameborder="0" allowfullscreen></iframe>`;
  });

  socket.addEventListener("close", () => {
    setupSocket();
  });
}

setupSocket();
